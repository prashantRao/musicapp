/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect } from 'react';
import { SafeAreaView, StatusBar } from 'react-native';
import AppRoute from './src/routes/appRoute'
import { commonStyle, color } from './src/common/styles'
import SplashScreen from 'react-native-splash-screen'

function App() {
  useEffect(() => {
    SplashScreen.hide()
  }, [])

  return (
    <SafeAreaView style={commonStyle.parentWrap}>
      <StatusBar backgroundColor={color.statusBar} />
      <AppRoute />
    </SafeAreaView>
  )
}

export default App;
