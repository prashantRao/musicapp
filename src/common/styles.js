import { StyleSheet } from 'react-native'

export const commonStyle = StyleSheet.create({
    parentWrap: {
        flex: 1, backgroundColor: 'white'
    }
})

export const font = {
    light: 'Montserrat-Light',
    regular: 'Montserrat-Regular',
    semibold: 'Montserrat-SemiBold',
    bold: 'Montserrat-Bold'
} 

export const color = {
    primary: '#466de2',
    statusBar: '#1538a0'
}