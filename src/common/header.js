import React from 'react'
import { View, Text, StyleSheet, Image, TouchableHighlight } from 'react-native'
import { font, color } from './styles'

function Header({ navigation, title, showBack }) {
    return (
        <View style={styles.wrapper}>
            {showBack ?
                <TouchableHighlight style={styles.menuBtn} underlayColor="transparent" onPress={() => navigation.goBack()}>
                    <Image source={require('./../../assets/images/back_icon.png')} style={styles.navIcon} />
                </TouchableHighlight>
                :
                <TouchableHighlight style={styles.menuBtn} underlayColor="transparent">
                    <Image source={require('./../../assets/images/menu_icon.png')} style={styles.navIcon} />
                </TouchableHighlight>
            }            
            <Text style={styles.titleText}>{title}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        height: 60, backgroundColor: color.primary, elevation: 5, flexDirection: 'row', alignItems: 'center'
    },
    titleText: {
        fontFamily: font.semibold, color: 'white', fontSize: 16
    },
    navIcon: {
        width: 16, height: 16
    },
    menuBtn: {
        paddingHorizontal: 15
    }
})

export default Header;