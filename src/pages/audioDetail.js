import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, TouchableHighlight, Image, Alert } from 'react-native';
import { font, color, commonStyle } from './../common/styles';
import Header from './../common/header';
import Video from 'react-native-video';
import Loading from './../components/loading';

function AudioDetail({ route, navigation }) {
    const { trackName, artworkUrl100, previewUrl } = route.params.info
    const [duration, setDuration] = useState('')
    const [audiostate, setAudiostate] = useState('paused')
    const [mode, setMode] = useState(true)
    const [currentime, setCurrentime] = useState('')
    const [progress, setProgress] = useState('0%')
    const [loading, setLoading] = useState(true)

    let audioPlayer = null

    const _audioAction = (type) => {
        if (type === 'play') {
            setAudiostate('playing')
            setMode(false)
        } else if (type === 'pause') {
            setAudiostate('paused')
            setMode(true)
        } else if (type === 'reload') {
            setAudiostate('playing')
            setMode(false)
        }
    }

    const _loadAudio = (value) => {
        setDuration(value.duration.toFixed(2))
        setLoading(false)
    }

    const _audioProgress = (value) => {
        setCurrentime(value.currentTime.toFixed(2))
        var progress = (value.currentTime * 100) / duration
        setProgress(`${progress}%`)
    }

    const _audioEnd = () => {
        setAudiostate('complete')
        setCurrentime(duration)
        audioPlayer.seek(0)
        setMode(true)
    }

    var controlBtn;
    switch (audiostate) {
        case 'paused':
            controlBtn = <TouchableHighlight underlayColor="transparent" onPress={() => _audioAction('play')}>
                <Image source={require('./../../assets/images/play_icon.png')} style={styles.controlIcon} />
            </TouchableHighlight>
            break;
        case 'playing':
            controlBtn = <TouchableHighlight underlayColor="transparent" onPress={() => _audioAction('pause')}>
                <Image source={require('./../../assets/images/pause_icon.png')} style={styles.controlIcon} />
            </TouchableHighlight>
            break;
        case 'complete':
            controlBtn = <TouchableHighlight underlayColor="transparent" onPress={() => _audioAction('reload')}>
                <Image source={require('./../../assets/images/reload_icon.png')} style={styles.controlIcon} />
            </TouchableHighlight>
            break;
    }

    return (
        <View style={commonStyle.parentWrap}>
            <Header title={trackName} showBack navigation={navigation} />
            <View style={styles.innerWrap}>
                <View style={styles.playerWrap}>
                    <Video
                        source={{ uri: previewUrl }}
                        ref={ref => audioPlayer = ref}
                        onLoad={_loadAudio}
                        style={styles.audioWrap}
                        audioOnly
                        poster={artworkUrl100}
                        resizeMode="cover"
                        paused={mode}
                        onEnd={_audioEnd}
                        onProgress={_audioProgress}
                    />
                    <View style={styles.trackWrap}>
                        <Text style={styles.timeText}>{currentime ? currentime : '00:00'} Sec</Text>
                        <View style={styles.timeTrack}>
                            <View style={[styles.activeTrack, { width: progress }]}></View>
                        </View>
                        <Text style={styles.timeText}>{duration} Sec</Text>
                    </View>
                    <View>
                        {controlBtn}
                    </View>
                </View>
                {loading && <View style={styles.loaderWrap}>
                    <Loading />
                </View>}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    innerWrap: {
        alignItems: 'center', paddingTop: 30, flex: 1
    },
    audioWrap: {
        width: 180, height: 180, borderRadius: 5, overflow: 'hidden', backgroundColor: '#cccccc30', borderWidth: 1, borderColor: '#cccccc30', elevation: 20, marginBottom: 50
    },
    musicImg: {
        width: '100%', height: '100%', resizeMode: 'cover'
    },
    timeTrack: {
        width: 180, height: 2, backgroundColor: '#cccccc50', marginHorizontal: 15
    },
    trackWrap: {
        flexDirection: 'row', alignItems: 'center', marginBottom: 30
    },
    timeText: {
        fontFamily: font.regular, fontSize: 12
    },
    activeTrack: {
        position: 'absolute', backgroundColor: color.primary, top: 0, bottom: 0
    },
    controlIcon: {
        width: 30, height: 30
    },
    playerWrap: {
        zIndex: 10, alignItems: 'center'
    },
    loaderWrap: {
        ...StyleSheet.absoluteFill, zIndex: 20, backgroundColor: 'white'
    }
})

export default AudioDetail;