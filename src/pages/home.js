import React, { useEffect, useState } from 'react'
import { View, Text, FlatList } from 'react-native'
import { commonStyle } from './../common/styles'
import { API_PATH } from './../common/paths'
import Header from './../common/header'
import ListView from './../components/listView'
import Loading from './../components/loading'

function Home({ navigation }) {
    const [songs, setSongs] = useState([])
    const [loading, setLoading] = useState(true)
    const [refreshing, setRefreshing] = useState(false)

    useEffect(() => {
        _fetchSongs()
    }, [])

    const _fetchSongs = (mode) => {
        fetch(`${API_PATH}/search?term=Michael+jackson`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(res => res.json())
            .then(res => {
                setSongs(res.results)
                setLoading(false)
                if (mode === 'refresh') {
                    setRefreshing(false)
                }
            })
            .catch(err => {
                console.log(err)
            })
    }

    const _refrehsPage = () => {
        setRefreshing(true)
        _fetchSongs('refresh')
    }

    return (
        <View style={commonStyle.parentWrap}>
            <Header title="Songs" showBack={false} navigation={navigation}/>
            {loading ?
                <Loading />
                :
                <FlatList
                    data={songs}
                    renderItem={(item, index) => <ListView data={item} navigation={navigation} />}
                    contentContainerStyle={{ padding: 15 }}
                    keyExtractor={item => item.trackId.toString()}
                    maxToRenderPerBatch={15}
                    removeClippedSubviews
                    refreshing={refreshing}
                    onRefresh={() => _refrehsPage()}
                />
            }
        </View>
    )
}

export default Home;