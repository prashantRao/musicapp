import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack'
import Home from './../pages/home'
import AudioDetail from './../pages/audioDetail'

const Stack = createStackNavigator()

function AppRoute() {
    return (
        <NavigationContainer>
            <Stack.Navigator headerMode="none" initialRouteName="Home" screenOptions={{ cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS }}>
                <Stack.Screen name="Home" component={Home} />
                <Stack.Screen name="AudioDetail" component={AudioDetail} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default AppRoute;