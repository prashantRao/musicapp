import React from 'react'
import { View, Text, StyleSheet, Image, TouchableHighlight } from 'react-native'
import { font, color } from './../common/styles'

function ListView({ data, navigation }) {
    const { trackName, artistName, primaryGenreName, artworkUrl100, collectionName } = data.item

    return (
        <TouchableHighlight underlayColor="transparent" style={styles.artWrap} onPress={() => navigation.navigate('AudioDetail', { info: data.item })}>
            <View style={styles.innerWrap}>
                <View style={styles.imgWrap}>
                    <Image source={{ uri: artworkUrl100 }} style={styles.artworkImg} />
                </View>
                <View style={styles.contentWrap}>
                    <View>
                        <Text style={styles.nameText}>{trackName}</Text>
                        <Text style={styles.infoText}>{collectionName}</Text>
                    </View>
                    <View style={styles.btmInfo}>
                        <Text style={[styles.infoText, styles.darkColor]}>{artistName}</Text>
                        <Text style={styles.sepText}>|</Text>
                        <Text style={[styles.infoText, styles.darkColor]}>{primaryGenreName}</Text>
                    </View>
                </View>
            </View>
        </TouchableHighlight>
    )
}

const styles = StyleSheet.create({
    artWrap: {
        marginBottom: 15, paddingBottom: 15, borderBottomWidth: 1, borderColor: '#cccccc30'
    },
    innerWrap: {
        flexDirection: 'row'
    },
    imgWrap: {
        width: 70, height: 70, borderRadius: 5, overflow: 'hidden', backgroundColor: '#cccccc30', borderWidth: 1, borderColor: '#cccccc30'
    },
    artworkImg: {
        width: '100%', height: '100%', resizeMode: 'cover'
    },
    nameText: {
        fontFamily: font.regular, fontSize: 14, color: color.primary
    },
    contentWrap: {
        marginLeft: 15, flex: 1, justifyContent: 'space-between'
    },
    infoText: {
        fontSize: 11, fontFamily: font.light, color: 'rgba(0,0,0,.5)', marginTop: 1
    },
    darkColor: {
        color: 'rgba(0,0,0,.8)'
    },
    btmInfo: {
        flexDirection: 'row', alignItems: 'center'
    },
    sepText: {
        fontFamily: font.light, fontSize: 10, marginHorizontal: 10, opacity: .3
    }
})

export default ListView;