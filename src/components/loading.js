import React from 'react'
import { View, Text, ActivityIndicator, StyleSheet } from 'react-native'
import { font, color, commonStyle } from './../common/styles'

function Loading() {
    return (
        <View style={[commonStyle.parentWrap, styles.loadingWrap]}>
            <ActivityIndicator size={20} color={color.primary} />
            <Text style={styles.loadText}>Please wait...</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    loadingWrap: {
        justifyContent: 'center', alignItems: 'center'
    },
    loadText: {
        fontFamily: font.regular, fontSize: 12, color: color.primary, marginTop: 10
    }
})

export default Loading;